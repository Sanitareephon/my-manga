import styled from "@emotion/styled";
import { Modal, SxProps, Theme, Container } from "@mui/material";
import React, { useEffect, useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter, Route, Routes, useLocation } from "react-router-dom";
import AuthorizationDialog from "./components/AuthorizationDialog";
import Header from "./components/Header";
import Subheader from "./components/Header/Subheader";
import { IManga } from "./hooks/IManga";
import { IUserProfile } from "./hooks/IUser";
import { useGetCurrentUser, useGetCurrentUserProfile } from "./hooks/useUser";
import FavoriteManga from "./pages/FavoriteManga";
import HomePage from "./pages/HomePage";
import MangaDetail from "./pages/MangaDetail";

const AppContent = () => {
  const search = useLocation().search;
  const queryParameterTitle = new URLSearchParams(search).get("title");
  const queryTitle = queryParameterTitle ? queryParameterTitle : "";

  const [currentManga, setCurrentManga] = useState<IManga | null>(null);
  const [searchValue, setSearchValue] = useState<string>(queryTitle);
  const [mangaList, setMangaList] = useState<IManga[]>([]);
  const [isOpenAuthorizeDialog, setIsOpenAuthorizeDialog] = useState(false);
  const [currentUser, setCurrentUser] = useState<string | null>(null);
  const [userProfile, setUserProfile] = useState<IUserProfile | null>(null);

  const { data: currentUserData, isSuccess: isGetCurrentUserSuccess } =
    useGetCurrentUser();
  const { data: currentProfile, refetch: refetchCurrentProfile } =
    useGetCurrentUserProfile();

  useEffect(() => {
    handleHeaderbarDisplay();
    if (
      !!currentUserData &&
      !!currentUserData.username &&
      isGetCurrentUserSuccess
    ) {
      setCurrentUser(currentUserData.username);
      refetchCurrentProfile();
    }
    if (!!currentProfile) {
      setUserProfile(currentProfile);
    }
  }, [
    currentUserData,
    currentProfile,
    isGetCurrentUserSuccess,
    refetchCurrentProfile,
  ]);

  /**
   * Toggle display headerbar
   */
  const handleHeaderbarDisplay = () => {
    let prevScrollpos = window.pageYOffset;
    let containerElement = document.getElementById("container");
    if (containerElement) {
      containerElement.onscroll = () => {
        containerElement = document.getElementById("container");
        const headerElement = document.getElementById("header");
        setTimeout(() => {
          if (containerElement && headerElement) {
            var currentScrollPos = containerElement.scrollTop;

            if (prevScrollpos > currentScrollPos) {
              headerElement.style.marginTop = "0";
            } else {
              headerElement.style.marginTop = "-83px";
            }
            prevScrollpos = currentScrollPos;
          }
        });
      };
    }
  };

  const homePage = (
    <HomePage
      searchValue={searchValue}
      setSearchValue={setSearchValue}
      setIsOpenAuthorizeDialog={setIsOpenAuthorizeDialog}
      currentUser={currentUser}
      userProfile={userProfile}
      mangaList={mangaList}
    />
  );

  const mangaDetailPage = (
    <MangaDetail
      currentManga={currentManga}
      setCurrentManga={setCurrentManga}
    />
  );

  const favoriteMangaPage = <FavoriteManga userProfile={userProfile} />;

  return (
    <AppWrapper>
      <Modal
        sx={modalStyle}
        open={isOpenAuthorizeDialog}
        onClose={() => setIsOpenAuthorizeDialog(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <div>
          <AuthorizationDialog
            setIsOpenAuthorizeDialog={setIsOpenAuthorizeDialog}
          ></AuthorizationDialog>
        </div>
      </Modal>

      <Header
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        setMangaList={setMangaList}
        setIsOpenAuthorizeDialog={setIsOpenAuthorizeDialog}
        currentUser={currentUser}
        setCurrentUser={setCurrentUser}
        setUserProfile={setUserProfile}
      />
      <Subheader
        searchValue={searchValue}
        currentManga={currentManga}
      ></Subheader>
      <ContainerWrapper id="container">
        <Container sx={containerStyle}>
          <Routes>
            <Route path={"/"} element={homePage}></Route>
            <Route path={"/search"} element={homePage}></Route>
            <Route path={"/manga/:mangaId"} element={mangaDetailPage}></Route>
            <Route path={"/favorite"} element={favoriteMangaPage}></Route>
          </Routes>
        </Container>
      </ContainerWrapper>
    </AppWrapper>
  );
};

const App = () => {
  const [queryClient] = useState<QueryClient>(new QueryClient());

  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <AppContent></AppContent>
      </BrowserRouter>
    </QueryClientProvider>
  );
};

export default App;

// Syle MUI Material Design
const modalStyle: SxProps<Theme> = {
  width: "100vw",
  height: "100vh",
};

const containerStyle: SxProps<Theme> = {
  height: "fit-content",
  width: "100vw",
  backgroundColor: "white",
  border: "1px solid #bdbdbd94",
};

// Styled component
const AppWrapper = styled.div`
  overflow: hidden;
  height: 100vh;
  width: 100vw;
`;

const ContainerWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  overflow: auto;
  background-color: #bbc8de54;
  height: calc(100vh - 64px);
`;
