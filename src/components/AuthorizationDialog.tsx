import {
  Alert,
  Box,
  Button,
  Paper,
  SxProps,
  Tab,
  Tabs,
  TextField,
  Theme,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  SIGNIN_SIGNUP_STATUS,
  useUserSignup,
  useUserSignin,
} from "../hooks/useUser";

interface AuthorizationDialogProps {
  setIsOpenAuthorizeDialog: React.Dispatch<React.SetStateAction<boolean>>;
}

const AuthorizationDialog: FC<AuthorizationDialogProps> = ({
  setIsOpenAuthorizeDialog,
}) => {
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [renderedErrorMessage, setRenderedErrorMessage] = useState(<div></div>);
  const [tabValue, setTabValue] = useState(0);
  const userSignin = useUserSignin();
  const userSignup = useUserSignup();

  // Signin useEffect
  useEffect(() => {
    switch (userSignin.authorizedStatus) {
      case SIGNIN_SIGNUP_STATUS.AUTHORIZED:
        setIsOpenAuthorizeDialog(false);
        break;
      case SIGNIN_SIGNUP_STATUS.UNAUTHORIZED:
        setRenderedErrorMessage(
          <Alert severity="error">Username or Password is incorrect.</Alert>
        );
        break;
    }
  }, [userSignin.authorizedStatus, navigate, setIsOpenAuthorizeDialog]);

  // Signup useEffect
  useEffect(() => {
    switch (userSignup.authorizedStatus) {
      case SIGNIN_SIGNUP_STATUS.AUTHORIZED:
        setIsOpenAuthorizeDialog(false);
        break;
      case SIGNIN_SIGNUP_STATUS.UNAUTHORIZED:
        setRenderedErrorMessage(
          <Alert severity="error">Username or Password is incorrect.</Alert>
        );
        break;
    }
  }, [userSignup.authorizedStatus, navigate, setIsOpenAuthorizeDialog]);

  const handleTabChange = (
    event: React.SyntheticEvent<Element, Event>,
    newValue: number
  ) => {
    setRenderedErrorMessage(<div></div>);
    setTabValue(newValue);
  };

  const handleSignInClick = () => {
    userSignin.signIn(username, password);
  };

  const handleSignUpClick = () => {
    userSignup.signup(username, password);
  };

  const handleOnSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (tabValue === 0) {
      handleSignInClick();
    } else {
      handleSignUpClick();
    }
  };

  return (
    <form onSubmit={(e) => handleOnSubmit(e)}>
      <Paper sx={paperStyle}>
        <Box sx={boxStyle}>
          <Tabs
            sx={tabsStyle}
            value={tabValue}
            onChange={handleTabChange}
            centered
          >
            <Tab sx={tabStyle} label="SIGN IN" />
            <Tab sx={tabStyle} label="SIGN UP" />
          </Tabs>

          <div className="log-in-form">
            <TextField
              sx={textFieldStyle}
              label="Username"
              value={username}
              variant="outlined"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="log-in-text-field-box">
            <TextField
              sx={textFieldStyle}
              label="Password"
              type="password"
              value={password}
              variant="outlined"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <Button sx={submitButtonStyle} variant="contained" type="submit">
              {tabValue === 0 ? "SIGN IN" : "SIGN UP"}
            </Button>
          </div>
          <div>{renderedErrorMessage}</div>
        </Box>
      </Paper>
    </form>
  );
};

export default AuthorizationDialog;

// Style MUI Material Design
const paperStyle: SxProps<Theme> = {
  margin: "auto",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "800px",
  height: "500px",
};

const boxStyle: SxProps<Theme> = {
  width: "500px",
  height: "330px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  margin: "auto",
};

const tabsStyle: SxProps<Theme> = {
  width: "100%",
};

const tabStyle: SxProps<Theme> = {
  width: "50%",
  fontWeight: "bold",
};

const textFieldStyle: SxProps<Theme> = {
  width: "100%",
};

const submitButtonStyle: SxProps<Theme> = {
  width: "100%",
  padding: "10px 0px",
  fontWeight: "bold",
};
