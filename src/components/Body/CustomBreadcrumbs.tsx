import { Breadcrumbs, Link, Stack, Typography } from "@mui/material";
import React, { Dispatch, FC, SetStateAction } from "react";
import { useNavigate } from "react-router-dom";

type LinkEvent =
  | React.MouseEvent<HTMLAnchorElement, MouseEvent>
  | React.MouseEvent<HTMLSpanElement, MouseEvent>;

interface CustomBreadcrumbsProps {
  searchValue: string;
  setSearchValue: Dispatch<SetStateAction<string>>;
}

interface IBreadcrumbsLink {
  breadcrumbsPath: string;
  breadcrumbsName: string;
}

const CustomBreadcrumbs: FC<CustomBreadcrumbsProps> = ({
  searchValue,
  setSearchValue,
}) => {
  const navigate = useNavigate();

  const breadcrumbsLink: IBreadcrumbsLink[] = [
    { breadcrumbsName: "Home", breadcrumbsPath: "/" },
    { breadcrumbsName: "Search", breadcrumbsPath: "/search?title=" },
  ];

  const handleLinkClick = (e: LinkEvent, link: IBreadcrumbsLink) => {
    e.preventDefault();
    const newSearchValue = link.breadcrumbsName === "Home" ? "" : searchValue;
    setSearchValue(newSearchValue);
    navigate(link.breadcrumbsPath);
  };

  const breadcrumbs = breadcrumbsLink.map((link, index) => (
    <Link
      underline="hover"
      key={index}
      color="inherit"
      href={link.breadcrumbsPath}
      onClick={(e) => handleLinkClick(e, link)}
    >
      {link.breadcrumbsName}
    </Link>
  ));

  return (
    <Stack spacing={2}>
      <Breadcrumbs separator="›">
        {breadcrumbs}
        <Typography key="3">{searchValue}</Typography>,
      </Breadcrumbs>
    </Stack>
  );
};

export default CustomBreadcrumbs;
