import styled from "@emotion/styled";
import { FavoriteBorderOutlined, FavoriteRounded } from "@mui/icons-material";
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  IconButton,
  SxProps,
  Theme,
  Typography,
} from "@mui/material";
import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from "react";
import { useQueryClient } from "react-query";
import { useNavigate } from "react-router-dom";
import { IManga } from "../../hooks/IManga";
import { IMangaCover } from "../../hooks/IMangaCover";
import { IUserProfile } from "../../hooks/IUser";
import useSearchMangaCoverId, {
  getCoverArtMangaId,
  getImagePath,
  getMangaCoverArtId,
} from "../../hooks/useSearchMangaCoverId";
import { useUpdateFavoriteManga } from "../../hooks/useUser";
import { getCache } from "../../utils/storeUtils";

interface MangaCardProps {
  mangaInfo: IManga;
  setIsOpenAuthorizeDialog: Dispatch<SetStateAction<boolean>>;
  currentUser: string | null;
  userProfile: IUserProfile | null;
}

const MangaCard: FC<MangaCardProps> = ({
  mangaInfo,
  setIsOpenAuthorizeDialog,
  currentUser,
  userProfile,
}) => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const updateFavoriteManga = useUpdateFavoriteManga();

  const coverArtId = getMangaCoverArtId(mangaInfo);
  const { data, refetch } = useSearchMangaCoverId(coverArtId);
  const [isFavorite, setIsFavorite] = useState(false);
  const [coverArtInfo, setCoverArtInfo] = useState<IMangaCover | undefined>();
  const cacheCoverArtId: IMangaCover | undefined = queryClient.getQueryData([
    "mangaCoverId",
    coverArtId,
  ]);

  const imagePath = coverArtInfo
    ? getImagePath(
        getCoverArtMangaId(coverArtInfo),
        coverArtInfo.attributes.fileName
      )
    : "";

  const setImageProperties = useCallback(() => {
    getCache(cacheCoverArtId, setCoverArtInfo, refetch);
  }, [cacheCoverArtId, refetch]);

  useEffect(() => {
    setImageProperties();
    if (userProfile) {
      const isFav =
        userProfile.favorite_list.filter((fav) => fav === mangaInfo.id).length >
        0;
      setIsFavorite(isFav);
    }
  }, [data, userProfile, mangaInfo, setImageProperties]);

  const handleFavoriteIconClick = () => {
    // Authorized user
    if (currentUser) {
      isFavorite
        ? updateFavoriteManga.removeFavorite(mangaInfo.id)
        : updateFavoriteManga.addFavorite(mangaInfo.id);
      setIsFavorite(!isFavorite);
    }
    // Not authorized user
    else {
      setIsOpenAuthorizeDialog(true);
    }
  };

  let favoriteElement = (
    <FavoriteBorderOutlined sx={favoriteOutlinedIconStyle} />
  );
  if (isFavorite) {
    favoriteElement = <FavoriteRounded sx={favoriteRoundedIconStyle} />;
  }

  return (
    <Card sx={cardStyle}>
      <CardMediaWrapper>
        <CardMedia
          sx={cardMediaStyle}
          component="img"
          image={imagePath}
          alt={mangaInfo.attributes.title.en}
          title={mangaInfo.attributes.title.en}
          onClick={() => navigate(`/manga/${mangaInfo.id}`)}
        />
      </CardMediaWrapper>
      <CardContent sx={cardContentStyle}>
        <Typography
          sx={typoGraphyContent}
          variant="body2"
          color="text.secondary"
          title={mangaInfo.attributes.title.en}
        >
          {mangaInfo.attributes.title.en}
        </Typography>
      </CardContent>

      <CardActions sx={cardActionsStyle}>
        <IconButton sx={favoriteIconStyle} onClick={handleFavoriteIconClick}>
          {favoriteElement}
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default MangaCard;

// Style MUI Material Design
const cardStyle: SxProps<Theme> = {
  width: "218px",
  height: "310px",
};

const cardMediaStyle: SxProps<Theme> = {
  objectFit: "fill",
  transition: "0.3s",
  height: "100%",

  "&:hover": {
    transform: "scale(1.2)",
    cursor: "pointer",
  },
};

const cardContentStyle: SxProps<Theme> = {
  padding: "9px 12px 3px 12px",
};

const typoGraphyContent: SxProps<Theme> = {
  whiteSpace: "nowrap",
  overflow: "hidden",
  textOverflow: "ellipsis",
};

const cardActionsStyle: SxProps<Theme> = {
  padding: "0px 5px",
  whiteSpace: "nowrap",
  overflow: "hidden",
  textOverflow: "ellipsis",
};

const favoriteIconStyle: SxProps<Theme> = {
  marginLeft: "auto",
};

const favoriteOutlinedIconStyle: SxProps<Theme> = {
  color: "#1976D2",
};

const favoriteRoundedIconStyle: SxProps<Theme> = {
  color: "#e91e63",
};

// Styled Component
const CardMediaWrapper = styled.div`
  overflow: hidden;
  width: 100%;
  height: 235px;
`;
