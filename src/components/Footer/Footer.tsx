import styled from "@emotion/styled";
import React from "react";

const Footer = () => {
  return (
    <FooterWrapper>
      <Copyright>Copyright © 2022. my-manga All Rights Reserved</Copyright>
    </FooterWrapper>
  );
};

export default Footer;

// Styled component
const FooterWrapper = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #aaaaaa;
  margin-top: 25px;
`;

const Copyright = styled.div`
  width: 100%;
  text-align: center;
`;
