import styled from "@emotion/styled";
import { DensitySmallSharp, FeedSharp } from "@mui/icons-material";
import { AppBar, Box, SxProps, Tab, Tabs, Theme, Toolbar } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import React, { Dispatch, FC, SetStateAction } from "react";
import { useNavigate } from "react-router-dom";
import Logo from "../../assets/logo.png";
import { IManga } from "../../hooks/IManga";
import { IUserProfile } from "../../hooks/IUser";
import ProfileAvatarIcon from "./ProfileAvatarIcon";
import SearchBar from "./SearchBar";

interface HeaderProps {
  searchValue: string;
  setSearchValue: Dispatch<SetStateAction<string>>;
  setMangaList: Dispatch<SetStateAction<IManga[]>>;
  setIsOpenAuthorizeDialog: React.Dispatch<React.SetStateAction<boolean>>;
  currentUser: string | null;
  setCurrentUser: Dispatch<SetStateAction<string | null>>;
  setUserProfile: Dispatch<SetStateAction<IUserProfile | null>>;
}

const Header: FC<HeaderProps> = ({
  searchValue,
  setSearchValue,
  setMangaList,
  setIsOpenAuthorizeDialog,
  currentUser,
  setUserProfile,
}) => {
  const navigate = useNavigate();

  const handleLogoClick = () => {
    setSearchValue("");
    navigate("/");
  };

  return (
    <Box sx={boxStyle} id="header">
      <AppBar sx={appBarStyle} id="header-bar-app-bar">
        <Toolbar sx={toolbarStlye}>
          <TabsWrapper>
            <ThemeProvider theme={theme}>
              <Tabs value={0} textColor="inherit" variant="fullWidth">
                <Tab sx={logoStyle} onClick={handleLogoClick}></Tab>
                <Tab icon={<DensitySmallSharp />} label="A-Z List" />
                {/* <Tab icon={<MenuBookRounded />} label="Book Mark" /> */}
                <Tab icon={<FeedSharp />} label="News" />
              </Tabs>
            </ThemeProvider>
          </TabsWrapper>
          <SearchBarWrapper>
            <SearchBar
              searchValue={searchValue}
              setSearchValue={setSearchValue}
              setMangaList={setMangaList}
            ></SearchBar>
          </SearchBarWrapper>
          <ProfileAvatarIcon
            setIsOpenAuthorizeDialog={setIsOpenAuthorizeDialog}
            currentUser={currentUser}
            setUserProfile={setUserProfile}
          />
        </Toolbar>
      </AppBar>
    </Box>
  );
};

Header.propTypes = {};

export default Header;

// Style MUI Material Design
const boxStyle: SxProps<Theme> = {
  minHeight: "85px",
  height: "85px",
  transition: "0.3s",
  backgroundColor: "#52688F !important;",
};
const appBarStyle: SxProps<Theme> = {
  position: "static",
  backgroundColor: "inherit",
};
const logoStyle: SxProps<Theme> = {
  background: `url(${Logo})`,
  backgroundSize: "contain",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};
const toolbarStlye: SxProps<Theme> = { gap: "30px" };

const theme = createTheme({
  palette: {
    primary: {
      main: "#00000000",
    },
  },
});

// Styled component
const TabsWrapper = styled.div`
  @media only screen and (max-width: 800px) {
    width: 0px;
  }
`;
const SearchBarWrapper = styled.div`
  height: 100%;
  min-width: 300px;
  flex: 1 1 auto;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
