export interface ISubheaderName {
    path: string,
    name: string
    icon?: JSX.Element
}