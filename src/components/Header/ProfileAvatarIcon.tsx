import styled from "@emotion/styled";
import { GradeSharp, LoginOutlined } from "@mui/icons-material";
import LogoutIcon from "@mui/icons-material/Logout";
import {
  Avatar,
  Box,
  Divider,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Popover,
  PopoverOrigin,
  SxProps,
  Theme,
  Typography,
} from "@mui/material";
import React, { Dispatch, FC, SetStateAction } from "react";
import { useNavigate } from "react-router-dom";
import { IUserProfile } from "../../hooks/IUser";
import { useUserSignout } from "../../hooks/useUser";

interface ProfileAvatarIconProps {
  setIsOpenAuthorizeDialog: React.Dispatch<React.SetStateAction<boolean>>;
  currentUser: string | null;
  setUserProfile: Dispatch<SetStateAction<IUserProfile | null>>;
}

const ProfileAvatarIcon: FC<ProfileAvatarIconProps> = ({
  setIsOpenAuthorizeDialog,
  currentUser,
  setUserProfile,
}) => {
  const navigate = useNavigate();
  const userSignout = useUserSignout();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleSignoutClick = () => {
    setAnchorEl(null);
    setUserProfile(null);
    userSignout.signOut();
  };

  const handleSigninClick = () => {
    setAnchorEl(null);
    setIsOpenAuthorizeDialog(true);
  };

  const handleFavoriteClick = () => {
    setAnchorEl(null);
    navigate("/favorite");
  };

  const renderedEditProfile = () => (
    <Typography variant="caption" title="Edit Profile">
      Edit Profile
    </Typography>
  );

  const renderedAuthorizeUserPopover = () => (
    <List component="nav">
      <ListItemButton onClick={() => setAnchorEl(null)}>
        <ListItemIcon>
          <Avatar alt="Avatar" sx={avatarPropStyle} />
        </ListItemIcon>
        <EditProfileWrapper>
          <ListItemText sx={usernameStyle} primary={currentUser} />
          <ListItemText primary={renderedEditProfile()} />
        </EditProfileWrapper>
      </ListItemButton>
      <Divider />
      <ListItemButton onClick={() => handleFavoriteClick()}>
        <ListItemIcon>
          <GradeSharp />
        </ListItemIcon>
        <ListItemText primary="Favorite" />
      </ListItemButton>
      <Divider />
      <ListItemButton onClick={() => handleSignoutClick()}>
        <ListItemIcon>
          <LogoutIcon />
        </ListItemIcon>
        <ListItemText primary="Sign out" />
      </ListItemButton>
    </List>
  );

  const renderedUnauthrizeUserPopover = () => (
    <List component="nav">
      <ListItemButton onClick={() => handleSigninClick()}>
        <ListItemIcon>
          <LoginOutlined />
        </ListItemIcon>
        <ListItemText primary="Sign in" />
      </ListItemButton>
    </List>
  );

  return (
    <div>
      <IconButton
        onClick={(e) => {
          setAnchorEl(e.currentTarget);
        }}
      >
        <Avatar alt="Avatar" sx={avatarPropStyle} />
      </IconButton>
      <Popover
        id="mouse-over-popover"
        open={!!anchorEl}
        anchorEl={anchorEl}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        onClose={() => setAnchorEl(null)}
        disableRestoreFocus
      >
        <Box className="pop-over-container" sx={popOverStyle}>
          {currentUser !== null
            ? renderedAuthorizeUserPopover()
            : renderedUnauthrizeUserPopover()}
        </Box>
      </Popover>
    </div>
  );
};

export default ProfileAvatarIcon;

// Style MUI Material Design
const popOverStyle: SxProps<Theme> = {
  maxWidth: "400px",
  width: "300px",
};

const anchorOrigin: PopoverOrigin = {
  vertical: "bottom",
  horizontal: "left",
};

const transformOrigin: PopoverOrigin = {
  vertical: "top",
  horizontal: "left",
};

const avatarPropStyle: SxProps<Theme> = {
  width: "56px",
  height: "56px",
};

const usernameStyle: SxProps<Theme> = {
  //   display: "flex",
  //   justifyContent: "center",
};

// Styled component
const EditProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 14px;
`;
