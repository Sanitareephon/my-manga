import styled from "@emotion/styled";
import SearchIcon from "@mui/icons-material/Search";
import {
  Divider,
  IconButton,
  InputBase,
  Paper,
  SxProps,
  Theme,
} from "@mui/material";
import React, {
  Dispatch,
  FC,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import { useNavigate } from "react-router-dom";
import { IManga } from "../../hooks/IManga";
import { useSearchManga } from "../../hooks/useSearchManga";

interface SearchBarProps {
  searchValue: string;
  setSearchValue: Dispatch<SetStateAction<string>>;
  setMangaList: Dispatch<SetStateAction<IManga[]>>;
}

const SearchBar: FC<SearchBarProps> = ({
  searchValue,
  setSearchValue,
  setMangaList,
}) => {
  const navigate = useNavigate();

  const [title, setTitlte] = useState<string>(searchValue);

  const { data } = useSearchManga(searchValue);

  useEffect(() => {
    const newData = data ? data.data : [];
    setMangaList(newData);
    setTitlte("");
  }, [data, setMangaList]);

  const handleFormSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    setSearchValue(title);
    navigate(`/search?title=${title}`);
  };

  return (
    <Wrapper>
      <div className="search-bar-field">
        <Paper component="form" sx={paperProp} onSubmit={handleFormSubmit}>
          <InputBase
            name="title"
            sx={inputBaseProp}
            value={title}
            onChange={(e) => {
              setTitlte(e.target.value);
            }}
            placeholder="Search Anime"
          />
          <Divider sx={dividerProp} orientation="vertical" />
          <IconButton id="search-button" type="submit" sx={iconProp}>
            <SearchIcon />
          </IconButton>
        </Paper>
      </div>
      <div className="search-bar-recent-search-box">
        {/* {renderedRecentSearchList()} */}
      </div>
    </Wrapper>
  );
};

export default SearchBar;

// Style MUI Material Design
const paperProp: SxProps<Theme> = {
  p: "2px 4px",
  display: "flex",
  alignItems: "center",
};
const inputBaseProp: SxProps<Theme> = { ml: 1, flex: 1 };
const dividerProp: SxProps<Theme> = { height: 28, m: 0.5 };
const iconProp: SxProps<Theme> = { p: "10px" };

// Styled component
const Wrapper = styled.div`
  width: 100%;
`;
