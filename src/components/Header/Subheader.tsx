import styled from "@emotion/styled";
import {
  AutoStoriesRounded,
  FavoriteRounded,
  HomeRounded,
  SearchRounded,
} from "@mui/icons-material";
import { Paper, SxProps, Theme, Typography } from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { IManga } from "../../hooks/IManga";

interface SubheaderProps {
  searchValue: string;
  currentManga: IManga | null;
}

const Subheader: FC<SubheaderProps> = ({ searchValue, currentManga }) => {
  const location = useLocation();
  const [subheaderDetail, setSubheaderDetail] = useState<JSX.Element>();

  useEffect(() => {
    let currentPath = "/";
    let title = "";
    const pathRegex = location.pathname.match(/(\/\w+|\/$)/);
    if (pathRegex) {
      currentPath = pathRegex[0];
      let renderedTitle = getRenderedTitle(<HomeRounded />, "Home");
      switch (currentPath) {
        case "/manga":
          title = currentManga ? currentManga.attributes.title.en : "";
          renderedTitle = getRenderedTitle(<AutoStoriesRounded />, title);
          break;
        case "/favorite":
          title = "Favorite";
          renderedTitle = getRenderedTitle(<FavoriteRounded />, "Favorite");
          break;
        case "/search":
          title = `Quick Search: "${searchValue}"`;
          renderedTitle = getRenderedTitle(<SearchRounded />, title);
          break;
        case "/":
          break;
        default:
          renderedTitle = getRenderedTitle(<></>, "");
          break;
      }
      setSubheaderDetail(renderedTitle);
    }
  }, [location.pathname, currentManga, searchValue]);

  const getRenderedTitle = (renderedIcon: JSX.Element, title: string) => (
    <TitleWrapper>
      <span>{renderedIcon}</span>
      <Typography sx={subheaderDetailStyle} variant="h4" component="div">
        {title}
      </Typography>
    </TitleWrapper>
  );

  return (
    <Paper sx={paperStyle} variant="outlined" square>
      {subheaderDetail}
    </Paper>
  );
};

export default Subheader;

// Style MUI Material Design
const paperStyle: SxProps<Theme> = {
  display: "flex",
  alignItems: "center",
  width: "100%",
  height: "60px",
  padding: "0px 24px",
  backgroundColor: "#192E41",
  flexGrow: 1,
};

const subheaderDetailStyle: SxProps<Theme> = {
  font: "24px arial,helvetica,clean,sans-serif",
  fontWeight: "bold",
};

// Styled component
const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 0px 15px;
  color: white;
`;
