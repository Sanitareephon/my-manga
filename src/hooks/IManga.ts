export type IMangaResponse = {
    result: string,
    response: string,
    data: IManga[],
    limit: number,
    offset: number,
    total: number
}

export type IMangaIdResponse = {
    result: string,
    response: string,
    data: IManga
}

export type IManga = {
    id: string,
    type: string,
    attributes: {
        title: {
            [key: string]: string
        },
        altTitles: [
            {
                [key: string]: string
            }
        ],
        description: {
            [key: string]: string
        },
        isLocked: true,
        links: {
            property1: string,
            property2: string
        },
        originalLanguage: string,
        lastVolume: string,
        lastChapter: string,
        publicationDemographic: string,
        status: string,
        year: number,
        contentRating: string,
        chapterNumbersResetOnNewVolume: true,
        availableTranslatedLanguages: [],
        tags: [
            {
                id: string,
                type: string,
                attributes: {
                    name: {
                        property1: string,
                        property2: string
                    },
                    description: {
                        property1: string,
                        property2: string
                    },
                    group: string,
                    version: number
                },
                relationships: [
                    {
                        id: string,
                        type: string,
                        related: string,
                        attributes: {}
                    }
                ]
            }
        ],
        state: string,
        version: number,
        createdAt: string,
        updatedAt: string
    },
    relationships: [
        {
            id: string,
            type: string,
            related: string,
            attributes: {}
        }
    ]
}