export type IMangaCoverResponse = {
    result: string,
    response: string,
    data: IMangaCover,
    limit: number,
    offset: number,
    total: number
}

export type IMangaCover =
    {
        id: string,
        type: string,
        attributes: {
            volume: string,
            fileName: string,
            description: string,
            locale: string,
            version: number,
            createdAt: string,
            updatedAt: string
        },
        relationships: [
            {
                id: string,
                type: string,
                related: string,
                attributes: {}
            }
        ]
    }