export interface IUser {
    id: string
    username: string;
    password: string;
}

export interface ICurrentUser {
    username: string | null;
}

export interface IUserProfile {
    id: string,
    username: string,
    favorite_list: string[],
    favorite_history_list: string[]
}