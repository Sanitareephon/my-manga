import { AxiosRequestConfig } from "axios"

export const hostname = "https://api.mangadex.org";
export const imageHostname = "https://uploads.mangadex.org";

export const getBaseMangaRequestConfig = () => {
    let baseMangaConfig: AxiosRequestConfig<any> = {
        baseURL: `${hostname}`,
    }
    return baseMangaConfig
}
