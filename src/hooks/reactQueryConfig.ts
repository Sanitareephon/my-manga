export const reactQueryOptions = {
    refetchOnWindowFocus: false,
    // enabled: false
}

export const disabledAutoSearchReactQueryOptions = {
    refetchOnWindowFocus: false,
    enabled: false
}
