import axios from 'axios';
import { useQuery } from 'react-query';
import { IMangaResponse } from './IManga';
import { getBaseMangaRequestConfig } from './mangaConfig';
import { reactQueryOptions } from './reactQueryConfig';

const getMangaConfigRequest = (title: string, offset: string) => {
    const mangaConfig = getBaseMangaRequestConfig();
    mangaConfig.url = "/manga";
    mangaConfig.params = { offset, limit: 50, "availableTranslatedLanguage[]": "en" };
    if (title.trim().length > 0) {
        mangaConfig.params.title = title;
    }
    return mangaConfig;
}

export const useSearchManga = (title: string = "", offset = "1") => {
    const mangaConfig = getMangaConfigRequest(title, offset);
    const mangaApiPromise = () => axios.request(mangaConfig).then((res) => res.data);

    return useQuery<IMangaResponse>(["manga", title], mangaApiPromise, reactQueryOptions);
};
