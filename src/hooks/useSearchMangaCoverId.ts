import axios from 'axios';
import { useQuery } from 'react-query';
import { IManga } from './IManga';
import { IMangaCover, IMangaCoverResponse } from './IMangaCover';
import { getBaseMangaRequestConfig, imageHostname } from './mangaConfig';
import { reactQueryOptions } from './reactQueryConfig';


const getMangaRequestConfig = (id: string) => {
    const mangaConfig = getBaseMangaRequestConfig();
    mangaConfig.url = `/cover/${id}`;
    return mangaConfig;
}

const useSearchMangaCoverId = (id: string) => {
    const mangaConfig = getMangaRequestConfig(id);
    const mangaApiPromise = () => axios.request(mangaConfig).then((res) => res.data)

    return useQuery<IMangaCoverResponse>(["mangaCoverId", id], mangaApiPromise, { ...reactQueryOptions, enabled: false })
};

// Utilities function
export const getMangaCoverArtId = (manga: IManga) => {
    return manga.relationships.filter((relationships) => relationships.type === "cover_art")[0].id;
}

export const getCoverArtMangaId = (coverInfo: IMangaCover) => {
    return coverInfo.relationships.filter((relationship) => relationship.type === "manga")[0].id
};

export const getImagePath = (mangaId: string, fileName: string) => `${imageHostname}/covers/${mangaId}/${fileName}`;

export default useSearchMangaCoverId;