import axios from 'axios';
import { useQuery } from 'react-query';
import { IMangaIdResponse } from './IManga';
import { getBaseMangaRequestConfig } from './mangaConfig';
import { disabledAutoSearchReactQueryOptions } from './reactQueryConfig';


const getMangaRequestConfig = () => {
    const mangaConfig = getBaseMangaRequestConfig();
    mangaConfig.url = `/manga`;
    return mangaConfig;
}

export const useSearchMangaId = (id: string = "") => {
    const mangaConfig = getMangaRequestConfig()
    mangaConfig.params += `/${id}`
    const mangaApiPromise = () => axios.request(mangaConfig).then((res) => res.data)

    return useQuery<IMangaIdResponse>(["mangaId", id], () => mangaApiPromise(), disabledAutoSearchReactQueryOptions)
};

export const useSearchMangaIdList = (params: string) => {
    const mangaConfig = getMangaRequestConfig()
    mangaConfig.params = params
    const mangaApiPromise = () => axios.request(mangaConfig).then((res) => res.data)

    return useQuery<IMangaIdResponse>(["mangaIdList"], () => mangaApiPromise(), disabledAutoSearchReactQueryOptions)
};
