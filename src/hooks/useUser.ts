import axios, { AxiosRequestConfig } from "axios";
import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useNavigate } from "react-router-dom";
import { ICurrentUser, IUser, IUserProfile } from "./IUser";
import { disabledAutoSearchReactQueryOptions, reactQueryOptions } from "./reactQueryConfig";

const hostname = "http://localhost:3100";

export enum SIGNIN_SIGNUP_STATUS {
    INITIAL = "initial",
    PROGRESS = "progress",
    AUTHORIZED = "authorized",
    UNAUTHORIZED = "unauthorized",
}

const getBaseUserConfig = () => {
    let baseUserConfig: AxiosRequestConfig<any> = {
        baseURL: `${hostname}`,
        headers: { "Content-type": "application/json; charset=UTF-8" },
    };
    return baseUserConfig;
};

export const useUpdateCurrentUser = () => {
    const navigate = useNavigate();
    const { refetch: currentUserRefetch } = useGetCurrentUser();
    const updateUserPromise = (currentUser: Omit<ICurrentUser, "current_user">) =>
        axios
            .request({
                ...getBaseUserConfig(),
                url: "current_user",
                method: "PUT",
                data: JSON.stringify(currentUser),
            })
            .then((res) => res.data);

    return useMutation<IUser, {}, any>(
        (currentUser: Omit<ICurrentUser, "current_user">) =>
            updateUserPromise(currentUser),
        {
            onSuccess: (e) => {
                currentUserRefetch();
                navigate("/");
                window.location.reload();
            },
        }
    );
};

export const useUserSignin = () => {
    const [authorizedStatus, setAuthorizedStatus] = useState(
        SIGNIN_SIGNUP_STATUS.INITIAL
    );
    const { data, refetch } = useUserList();
    const { mutate } = useUpdateCurrentUser();

    useEffect(() => {
        refetch();
    }, [refetch]);

    const signIn = (username: string, password: string) => {
        refetch();
        updateSigninState(username, password);
    };

    const updateSigninState = (username: string, password: string) => {
        setAuthorizedStatus(SIGNIN_SIGNUP_STATUS.PROGRESS);
        if (data) {
            const isFoundUser =
                data.filter((u) => u.username === username && u.password === password)
                    .length > 0;
            const authorizedStatus = isFoundUser
                ? SIGNIN_SIGNUP_STATUS.AUTHORIZED
                : SIGNIN_SIGNUP_STATUS.UNAUTHORIZED;
            setAuthorizedStatus(authorizedStatus);
            if (isFoundUser) {
                mutate({ username });
            }
        }
    };

    return { authorizedStatus, signIn, STATUS: SIGNIN_SIGNUP_STATUS };
};

export const useUserSignout = () => {
    const { mutate } = useUpdateCurrentUser();
    const signOut = () => mutate({ username: null });

    return { signOut };
};

export const useUserSignup = () => {
    const { data, refetch } = useUserList();
    const { mutate } = useAddUser();
    const { mutate: currentUserMutate } = useUpdateCurrentUser();
    const [authorizedStatus, setAuthorizedStatus] = useState(
        SIGNIN_SIGNUP_STATUS.INITIAL
    );

    useEffect(() => {
        refetch();
    }, [refetch]);

    const signup = (username: string, password: string) => {
        setAuthorizedStatus(SIGNIN_SIGNUP_STATUS.PROGRESS);
        refetch();
        if (data) {
            const isDuplicateUser =
                data.filter((u) => u.username === username).length > 0;
            if (isDuplicateUser) {
                setAuthorizedStatus(SIGNIN_SIGNUP_STATUS.UNAUTHORIZED);
            } else if (!password) {
                setAuthorizedStatus(SIGNIN_SIGNUP_STATUS.UNAUTHORIZED);
            } else {
                mutate({ username, password });
                currentUserMutate({ username });
                setAuthorizedStatus(SIGNIN_SIGNUP_STATUS.AUTHORIZED);
                refetch();
            }
        }
    };

    return { authorizedStatus, signup };
};

export const useGetCurrentUser = () => {
    const userConfig = getBaseUserConfig();
    userConfig.url = "current_user";
    const currentUserPromise = () =>
        axios.request(userConfig).then((res) => res.data);

    return useQuery<ICurrentUser>("current_user", currentUserPromise, reactQueryOptions);
};

export const useUpdateFavoriteManga = () => {
    const currentUser = useGetCurrentUser();
    const currentUserProfile = useGetCurrentUserProfile();
    const putFavoriteManga = usePutFavoriteManga();
    const postFavoriteManga = usePostFavoriteManga();

    const addFavorite = (mangaId: string) => {
        // Have a favorite list in db.json
        if (currentUserProfile && currentUserProfile.data) {
            const userPofile = currentUserProfile.data;
            if (userPofile) {
                const newFavoriteManga = {
                    ...userPofile,
                    favorite_list: [...userPofile.favorite_list, mangaId],
                };
                putFavoriteManga.mutate(newFavoriteManga);
            }
        }
        // New user (Doesn't has a favorite list in db.json)
        else if (currentUser && currentUser.data && currentUser.data.username) {
            const newFavoriteManga: IUserProfile = {
                id: currentUser.data.username,
                username: currentUser.data.username,
                favorite_list: [mangaId],
                favorite_history_list: [],
            };
            postFavoriteManga.mutate(newFavoriteManga);
        }
    };

    const removeFavorite = (mangaId: string) => {
        if (currentUserProfile && currentUserProfile.data) {
            const userProfile = currentUserProfile.data;
            if (currentUserProfile) {
                const newFavList = userProfile.favorite_list.filter((fav) => fav !== mangaId);
                const isExistHistory = userProfile.favorite_history_list.filter((his) => his === mangaId)
                const newHistory = isExistHistory ? [...userProfile.favorite_history_list] : [...userProfile.favorite_history_list, mangaId];
                const newFavoriteManga: IUserProfile = {
                    ...userProfile,
                    favorite_list: newFavList,
                    favorite_history_list: [...newHistory]
                };
                putFavoriteManga.mutate(newFavoriteManga);
            }
        }
    };
    return { addFavorite, removeFavorite };
};

const usePutFavoriteManga = () => {
    const currentUserProfile = useGetCurrentUserProfile();
    const updateUserPromise = (
        currentUser: Omit<IUserProfile, "update_user_profile">
    ) =>
        axios
            .request({
                ...getBaseUserConfig(),
                url: `user_profile/${currentUser.username}`,
                method: "PUT",
                data: JSON.stringify(currentUser),
            })
            .then((res) => res.data);

    return useMutation<IUserProfile, {}, any>(
        (currentUser: Omit<IUserProfile, "update_user_profile">) =>
            updateUserPromise(currentUser),
        {
            onSuccess: (e) => {
                currentUserProfile.refetch();
            },
        }
    );
};

const usePostFavoriteManga = () => {
    const currentUserProfile = useGetCurrentUserProfile();
    const updateUserPromise = (
        currentUser: Omit<IUserProfile, "post_user_profile">
    ) =>
        axios
            .request({
                ...getBaseUserConfig(),
                url: `user_profile`,
                method: "POST",
                data: JSON.stringify(currentUser),
            })
            .then((res) => res.data);

    return useMutation<IUserProfile, {}, any>(
        (currentUser: Omit<IUserProfile, "post_user_profile">) =>
            updateUserPromise(currentUser),
        {
            onSuccess: (e) => {
                currentUserProfile.refetch();
            },
        }
    );
};
export const useGetCurrentUserProfile = () => {
    const { data: userCurrentData } = useGetCurrentUser();
    const userConfig = getBaseUserConfig();
    userConfig.url = `user_profile/${userCurrentData?.username}`;
    const currentUserPromise = () =>
        axios.request(userConfig).then((res) => res.data);

    return useQuery<IUserProfile>(["user_profile", userCurrentData], currentUserPromise, disabledAutoSearchReactQueryOptions);
};

// Helper hook
const useAddUser = () => {
    const userConfig = (newUser: Omit<IUser, "username">) => {
        return {
            ...getBaseUserConfig(),
            url: "user_list",
            method: "POST",
            data: JSON.stringify(newUser),
        } as AxiosRequestConfig<any>;
    };
    const addUserPromise = (newUser: Omit<IUser, "username">) =>
        axios.request(userConfig(newUser)).then((res) => res.data);

    return useMutation<IUser, {}, any>(
        (newUser: Omit<IUser, "username">) => addUserPromise(newUser),
        { onSuccess: (e) => { } }
    );
};

const useUserList = () => {
    const userConfig = getBaseUserConfig();
    userConfig.url = "user_list";
    const userListPromise = () =>
        axios.request(userConfig).then((res) => res.data);

    return useQuery<IUser[]>("user_list", userListPromise);
};
