import React, { useState } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Backdrop, CircularProgress } from "@mui/material";
import axios from "axios";

const AxiosRequestHandler = () => {
  const [isLoading, setIsLoading] = useState(false);

  axios.interceptors.request.use(
    (config) => {
      if (config.url && !config.url.includes("user_profile")) {
        setIsLoading(true);
      }
      return config;
    },
    (error) => {
      // alert("Found some error" + error);
      return Promise.reject(error);
    }
  );

  // Add a response interceptor
  axios.interceptors.response.use(
    (response) => {
      setIsLoading(false);
      return response;
    },
    (error) => {
      // alert("Found some error" + error);
      console.log(JSON.stringify(error));
      return Promise.reject(error);
    }
  );

  return (
    <Backdrop
      sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={isLoading}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <App />
    <AxiosRequestHandler />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
