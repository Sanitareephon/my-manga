import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import {
  Avatar,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  SxProps,
  Theme,
} from "@mui/material";
import React, { FC } from "react";
import Footer from "../components/Footer";
import { IUserProfile } from "../hooks/IUser";

interface FavoriteMangaProp {
  userProfile: IUserProfile | null;
}

const FavoriteManga: FC<FavoriteMangaProp> = ({ userProfile }) => {
  const getMangaParams = (userProfile: string[]) => {
    let params = "";
    userProfile.forEach((mangaId) => {
      params += `ids[]=${mangaId}&`;
    });
    return params;
  };

  return (
    <>
      <List sx={listStyle}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <ImageIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Photos" secondary={""} />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <WorkIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Work" secondary="Jan 7, 2014" />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <BeachAccessIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Vacation" secondary="July 20, 2014" />
        </ListItem>
      </List>
      <Footer />
    </>
  );
};

export default FavoriteManga;

// Style MUI Material Design
const listStyle: SxProps<Theme> = {
  width: "100%",
  bgcolor: "background.paper",
  minHeight: "calc(100vh - 290px)",
};
