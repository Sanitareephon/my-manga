import styled from "@emotion/styled";
import React, { Dispatch, FC, SetStateAction, useEffect } from "react";
import CustomBreadcrumbs from "../components/Body/CustomBreadcrumbs";
import MangaCard from "../components/Body/MangaCard";
import Footer from "../components/Footer";
import { IManga } from "../hooks/IManga";
import { IUserProfile } from "../hooks/IUser";
import { scrollToTop } from "../utils/htmlUtils";

interface HomePageProps {
  searchValue: string;
  mangaList: IManga[];
  currentUser: string | null;
  userProfile: IUserProfile | null;

  setSearchValue: Dispatch<SetStateAction<string>>;
  setIsOpenAuthorizeDialog: Dispatch<SetStateAction<boolean>>;
}

const HomePage: FC<HomePageProps> = ({
  searchValue,
  setSearchValue,
  setIsOpenAuthorizeDialog,
  currentUser,
  mangaList,
  userProfile,
}) => {
  useEffect(() => {
    scrollToTop();
  }, []);

  const renderedMangaCard = () => {
    return mangaList.map((data, index) => (
      <MangaCard
        key={index}
        mangaInfo={data}
        setIsOpenAuthorizeDialog={setIsOpenAuthorizeDialog}
        currentUser={currentUser}
        userProfile={userProfile}
      ></MangaCard>
    ));
  };

  return (
    <>
      <CustomBreadcrumbs
        searchValue={searchValue}
        setSearchValue={setSearchValue}
      />
      <MangaCardWrapper>{renderedMangaCard()}</MangaCardWrapper>
      <Footer />
    </>
  );
};

export default HomePage;

// Styled component
const MangaCardWrapper = styled.div`
  display: flex;
  gap: 15px;
  justify-content: flex-start;
  flex-wrap: wrap;
  min-height: calc(100vh - 290px);
`;
