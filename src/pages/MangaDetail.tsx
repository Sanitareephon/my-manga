import styled from "@emotion/styled";
import React, { Dispatch, FC, SetStateAction, useEffect } from "react";
import { useQueryClient } from "react-query";
import { useParams } from "react-router-dom";
import Footer from "../components/Footer";
import { IManga } from "../hooks/IManga";
import { useSearchMangaId } from "../hooks/useSearchMangaId";
import { scrollToTop } from "../utils/htmlUtils";
import { getCache } from "../utils/storeUtils";

interface MangaDetailProps {
  currentManga: IManga | null;
  setCurrentManga: Dispatch<SetStateAction<IManga | null>>;
}

const MangaDetail: FC<MangaDetailProps> = ({
  currentManga,
  setCurrentManga,
}) => {
  const { mangaId } = useParams();
  const queryClient = useQueryClient();

  const { data, refetch } = useSearchMangaId(mangaId);

  useEffect(() => {
    const cacheMangaResponse = queryClient.getQueryData(["mangaId", mangaId]);
    getCache(cacheMangaResponse, setCurrentManga, refetch);
    scrollToTop();

    if (data && data.data.id === mangaId) {
      setCurrentManga(data.data);
    } else {
      setCurrentManga(null);
    }
  }, [data, mangaId, queryClient, refetch, setCurrentManga]);

  return (
    <>
      Manga Detail
      <MangaContentWrapper>
        <div>{JSON.stringify(currentManga)}</div>
      </MangaContentWrapper>
      <Footer></Footer>
    </>
  );
};

export default MangaDetail;

// Styled component
const MangaContentWrapper = styled.div`
  min-height: calc(100vh - 290px);
  width: 100%;
`;
