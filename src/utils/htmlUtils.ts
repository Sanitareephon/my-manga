export const scrollToTop = () => {
    const containerElement = document.querySelector("#container");

    if (containerElement) {
        containerElement.scrollTo(0, 0);
    }
}