/**
 *
 * @param cacheData Cache data from reqact query
 * @param setCache
 * @param triggerRefetch
 */
export const getCache = (
    cacheData: any,
    setCache: Function,
    triggerRefetch: Function
) => {
    if (!!cacheData) {
        setCache(cacheData.data);
    } else {
        triggerRefetch();
    }
};